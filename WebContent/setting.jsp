<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>${editUser.name}のユーザー編集</title>
		<link rel="stylesheet" href="./css/stylesheet.css" type="text/css">
	</head>
	<body>
		<div class="wrap">
			<header>
				<a href="./"><img src="./image/logo.png" alt="ホーム画面" class="logo"></a>
				<div class="header-botton">
					<a href="manage">管理画面</a>
				</div>
			</header>

			<c:if test = "${not empty errorMessages}" >
				<div class = "error-messages">
					<ul>
						<c:forEach items = "${errorMessages}" var = "message">
							<li><c:out value = "${message}" /></li>
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<div class="setting-contents">
				<form action="setting" method="post"><br />
					<input name="id" value="${editUser.id}" id="id" type="hidden"/>
					<label for="account">ログインID</label>
	                <input name="account" value="${editUser.account}" /><br />
					<label for="name">名前</label>
	                <input name="name" value="${editUser.name}" id="name"/><br />
	                <label for="password">パスワード</label>
	                <input type="password" name="password" id="password"/><br />
	                <label for="passwordCheck">パスワード(確認用)</label>
					<input name="passwordCheck" id="passwordCheck" type="password"><br />

					<c:if test="${editUser.id != loginUser.id}">
						<select name="branchId">
							<c:forEach items="${branches}" var="branch">
								<c:if test="${editUser.branchId == branch.id}">
									<option selected value="${branch.id}"><c:out value="${branch.name}"></c:out></option>
								</c:if>
								<c:if test="${editUser.branchId != branch.id}">
									<option value="${branch.id}"><c:out value="${branch.name}"></c:out></option>
								</c:if>
							</c:forEach>
						</select>
						<select name="departmentId">
							<c:forEach items="${departments}" var="department">
								<c:if test="${editUser.departmentId == department.id}">
									<option selected value="${department.id }"><c:out value="${department.name}"></c:out></option>
								</c:if>
								<c:if test="${editUser.departmentId != department.id}">
									<option value="${department.id}"><c:out value="${department.name}"></c:out></option>
								</c:if>
							</c:forEach>
						</select>
					</c:if>

					<c:if test="${editUser.id == loginUser.id}">
						<input type="hidden" name="branchId" value="${loginUser.branchId}">
						<input type="hidden" name="departmentId" value="${loginUser.departmentId}">
					</c:if>
					<input type="submit" value="編集">
				</form>
			</div>
		</div>
	</body>
</html>