<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link rel="stylesheet" href="./css/stylesheet.css" type="text/css">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	</head>
	<body>
		<div class="wrap">
			<script src="./jQuery/jQuery.js"></script>
			<header>
				<a href="./"><img src="./image/logo.png" alt="ホーム画面" class="logo"></a>
				<div class="header-botton">
					<a href="./">ホーム</a>
					<a href="signup">新規登録</a>
				</div>
			</header>
		<br />

		<c:if test="${not empty errorMessages}">
				<div class="error-messages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"></c:remove>
			</c:if>

		<div class="users">
			<c:if test="${not empty users}">
				<c:forEach items="${users}" var="user">
					<div class="user">
						<div class="user-account-name">
							<span class="account"><c:out value="${user.account}" /></span>
							<span class="name"><c:out value="${user.name}" /></span>
						</div>
						<c:forEach items="${branches}" var="branch">
							<c:if test="${user.branchId == branch.id}">
								<div class="branch-id"><c:out value="${branch.name}" /></div>
							</c:if>
						</c:forEach>
						<c:forEach items="${departments}" var="department">
							<c:if test="${user.departmentId == department.id}">
								<div class="department-id"><c:out value="${department.name}" /></div>
							</c:if>
						</c:forEach>
						<div class="is-stopped">
							★現在のステータス<br />
							<c:if test="${user.isStopped == 0}">
								<c:out value="アクティブ"></c:out>
							</c:if>
							<c:if test="${user.isStopped == 1}">
								<c:out value="停止中"></c:out>
							</c:if>
						</div>
						<div class="stop-button">
							<c:if test="${user.id != loginUser.id}">
								<form name="isStopped" action="isStopped" method="post">
									<input type="hidden" name="userId" value="${user.id}">
									<c:if test="${user.isStopped == 0}">
										<input type="hidden" name="isStopped" value="1">
										<input type="submit" name="statusButton" value="アカウントを停止する" onclick="return confirm('ほんとうにステータスを変更してよろしいですか？')">
									</c:if>
									<c:if test="${user.isStopped == 1}">
										<input type="hidden" name="isStopped" value="0">
										<input type="submit" name="statusButton" value="アカウントを再開する" onclick="return confirm('ほんとうにステータスを変更してよろしいですか？')">
									</c:if>
								</form>
							</c:if>
						</div>
						<div class="date"><fmt:formatDate value="${user.createdDate}" pattern = "yyyy/MM/dd HH:ss"  /></div>
						<form action="setting">
							<input type="hidden" name="userId" value="${user.id}">
							<input type="submit" value="編集">
						</form><br />
					</div>
				</c:forEach>
			</c:if>
		</div>
		<p class="pagetop"><a href="#wrap">▲</a></p>
		</div>
	</body>
</html>