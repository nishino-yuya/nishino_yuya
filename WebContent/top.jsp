<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板課題ホーム</title>
		<link rel="stylesheet" href="./css/stylesheet.css" type="text/css">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	</head>
	<body>
		<script src="./jQuery/jQuery.js"></script>
		<div class="wrap">
			<header>
				<a href="./"><img src="./image/logo.png" alt="ホーム画面" class="logo"></a>
				<div class="header-botton">
					<a href="message">新規投稿</a>
					<c:if test="${loginUser.branchId == 1 and loginUser.departmentId == 1}">
						<a href="manage">ユーザー管理</a>
					</c:if>
					<c:if test="${not empty search[0] and empty search[1]}">
						<a href="./">すべての投稿</a>
					</c:if>
					<c:if test="${empty search[0] and not empty search[1]}">
						<a href="./">すべての投稿</a>
					</c:if>
					<c:if test="${not empty search[0] and not empty search[1]}">
						<a href="./">すべての投稿</a>
					</c:if>
					<c:if test="${not empty search[2]}">
						<a href="./">すべての投稿</a>
					</c:if>
					<div class="logout">
						<a href="logout">ログアウト</a>
					</div>
					<br />
				</div>
			</header>

			<c:if test="${not empty adminErrorMessage}">
				<div class="error-messages">
					<c:out value="${adminErrorMessage}"></c:out>
					<c:remove var="adminErrorMessage" scope="session" />
				</div>
			</c:if>
			<c:if test = "${not empty errorMessages}" >
				<div class="error-messages">
					<ul>
						<c:forEach items = "${errorMessages}" var = "errorMessage">
							<li><c:out value = "${errorMessage}" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="search">
				<form action="./">
					<div class="search-date">
						日付けから選択<br />
						<input type="date" name="startDate" min="2019-01-01" value="${search[0]}">～<input type="date" name="endDate" min="2019-01-01" value="${search[1]}">
					</div>
					<div class="search-category">
						カテゴリーから選択<br />
						<input name="category" id="category" value="${search[2]}">
					</div>
					<div class="search-submit"><input type="submit" value="検索する"></div>
				</form>
			</div>

			<div class="top-contents">
				<c:forEach items= "${messages}" var="message">
					<div class="message">
						<div class="account-name">
							<c:out value="${message.account}" />
							<c:out value="${message.name}" />
						</div>
						件名<br />
						<div class="title"><c:out value="${message.title}" /></div>
						カテゴリー<br />
						<div class="category"><c:out value="${message.category}" /></div>
						本文<br />
						<div class="text"><pre style="font-family: 'Times New Roman';"><c:out value="${message.text}" /></pre></div>
						<div class="date"><fmt:formatDate value="${message.createdDate}" pattern = "yyyy/MM/dd HH:mm"  /></div>
						<c:if test="${message.userId == loginUser.id}">
							<form action="deleteMessage" method="post">
								<input type="hidden" name="deleteMessage" value="${message.id}">
								<input type="submit" name="deleteButtun" value="削除する" onclick="return confirm('ほんとうに削除してよろしいですか？')">
							</form>
						</c:if>
					</div>
				<div class="comment">
					<c:forEach items="${comments}" var="comment">
						<c:if test="${message.id == comment.messageId}">
							<div class="comment-account-name">
								<br />
								<c:out value="${comment.account}" />
								<c:out value="${comment.name}" />
							</div>
							<div class="text"><pre style="font-family: 'Times New Roman';"><c:out value="${comment.text}" /></pre></div>
							<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern = "yyyy/MM/dd HH:mm"  /></div>
							<c:if test="${comment.userId == loginUser.id}">
								<form action="deleteComment" method="post">
									<input type="hidden" name="deleteComment" value="${comment.id}">
									<input type="submit" name="deleteButtun" value="削除する" onclick="return confirm('ほんとうに削除してよろしいですか？')">
								</form>
							</c:if>
						</c:if>
					</c:forEach>
				</div>
				<br />
				<div class="comment-form-area">
					<form action="comment" method="post">
						コメント投稿<br />
						<textarea name="comment" cols="50" rows="10" wrap="soft"></textarea>
						<input type="hidden" name="messageId" value="${message.id}">
						<input type="submit" value="コメントする">
					</form>
				</div>
				</c:forEach>
			</div>
			<p class="pagetop"><a href="#wrap">▲</a></p>
			<div class="copyright"> Copyright(c) NishinoYuya</div>
		</div>
	</body>
</html>