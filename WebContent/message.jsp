<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link rel="stylesheet" href="./css/stylesheet.css" type="text/css">
	</head>
	<body>
		<script type="text/javascript" src="./jQuery/jQuery.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js">
		</script>
		<div class="wrap">
			<header>
				<a href="./"><img src="./image/logo.png" alt="ホーム画面" class="logo"></a>
			</header>

			<c:if test = "${not empty errorMessages}" >
				<div class = "error-messages">
					<ul>
						<c:forEach items = "${errorMessages}" var = "message">
							<li><c:out value = "${message}" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="message-contents">
				<div class="new-form-area">
					<form action="message" method="post">
						件名<br />
						<input name="title" value="${message.title}"><br />
						本文<br />
						<textarea name="text" cols="100" rows="10" wrap="soft"><c:out value="${message.text}"></c:out></textarea><br />
						カテゴリー<br />
						<input name="category" value="${message.category}"><br />
						<input type="submit" value="投稿">
					</form>
				</div>
			</div>
		</div>
	</body>
</html>