<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規ユーザー登録</title>
		<link rel="stylesheet" href="./css/stylesheet.css" type="text/css">
	</head>
	<body>
		<div class = "wrap">
			<a href="./"><img src="./image/logo.png" alt="ホーム画面" class="logo"></a>
			<header>
				<div class="header-botton">
					<a href="manage">管理画面</a>
				</div>
			</header>

			<!-- 入力にミスがあればエラーメッセージを表示 -->
			<c:if test = "${not empty errorMessages}" >
				<div class = "error-messages">
					<ul>
						<c:forEach items = "${errorMessages}" var = "message">
							<li><c:out value = "${message}" /></li>
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<!-- 入力フォームの作成 -->
			<div class="signup-contents">
				<form action="signup" method="post"><br />
					<label for="account">ログインID</label>
					<input name="account" id="account" value="${user.account}"><br />
					<label for="name">名前</label>
					<input name="name" id="name" value="${user.name}"><br />
					<label for="password">パスワード</label>
					<input name="password" id="password" type="password"><br />
					<label for="passwordCheck">パスワード(確認用)</label>
					<input name="passwordCheck" id="passwordCheck" type="password"><br />

					<select name="branchId">
						<c:if test="${empty user.branchId}">
							<option value="100" selected>---------</option>
						</c:if>
						<c:forEach items="${branches}" var="branch">
							<option value="${branch.id}"
							<c:if test="${user.branchId == branch.id}">selected</c:if>
							><c:out value="${branch.name}"></c:out></option>
						</c:forEach>
					</select>
					<select name="departmentId">
						<c:if test="${empty user.departmentId}">
							<option value="100" selected>---------</option>
						</c:if>
						<c:forEach items="${departments}" var="department">
							<option value="${department.id}"
							<c:if test="${user.departmentId == department.id}">selected</c:if>
							><c:out value="${department.name}"></c:out></option>
						</c:forEach>
					</select>
					<input type="submit" value="新規登録">
				</form>
			</div>
		</div>
	</body>
</html>