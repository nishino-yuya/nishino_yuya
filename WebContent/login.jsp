<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン</title>
		<link rel="stylesheet" href="./css/stylesheet.css" type="text/css">
	</head>
	<body>
		<div class="wrap">
			<c:if test="${not empty loginErrorMessage and empty errorMessages}">
				<div class="error-messages">
					<c:out value="${loginErrorMessage}"></c:out>
				</div>
			</c:if>
			<c:if test="${not empty errorMessages}">
				<div class="error-messages">
					<ul>
						<c:forEach items="${errorMessages}" var ="message">
							<li><c:out value = "${message}" /></li>
						</c:forEach>
					</ul>
				</div>
			</c:if>
			<c:remove var="errorMessages" scope="session" />

			<div class="login-contents">
				<form action="login" method="post">
					<label for="account">ログインID</label><br />
					<input name="account" id="account" value="${account}"><br />
					<label for="password">パスワード</label><br />
					<input name="password" id="password" type="password"><br />
					<input type="submit" value="ログイン">
				</form>
			</div>
		</div>
	</body>
</html>