package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtils.*;

import java.sql.Connection;

import board.beans.Comment;
import board.dao.CommentDao;

public class CommentService {

	public void register(Comment comment) {

		Connection connection = null;

		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public void deleteComment(int commentId) {

		Connection connection = null;

		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, commentId);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}
}
