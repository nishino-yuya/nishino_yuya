package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtils.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.beans.UserComment;
import board.dao.UserCommentDao;
import board.dao.UserDao;
import board.utils.CipherUtil;

public class UserService {

	// 入力された情報をサーブレットから受け取り、整形してSQｌに渡す
	public void register(User user) {


		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());

			user.setPassword(encPassword);

			UserDao userDao = new UserDao();

			// SQL文に挿入
			userDao.insert(connection, user);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public void changeStatus(User user) {

		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.changeStatus(connection, user);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public User login(String account, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, account, encPassword);

			commit(connection);

			return user;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public User accountCheck(String account) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.accountCheck(connection, account);

			commit(connection);

			return user;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<User> getUsers(){

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<User> ret = userDao.getUsers(connection, LIMIT_NUM);

			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public List<String> getUsersAccounts(){

		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<String> ret = userDao.getUsersAccounts(connection, LIMIT_NUM);

			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public User getEditUser(int editUserId) {

		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User ret = userDao.getEditUser(connection, editUserId);

			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public List<UserComment> getComment(){

		Connection connection = null;

		try {
			connection = getConnection();

			UserCommentDao CommentDao = new UserCommentDao();
			List<UserComment> ret = CommentDao.getUserComments(connection, LIMIT_NUM);

			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public void update(User editUser) {

		Connection connection = null;

		try {
			connection = getConnection();

			if(!StringUtils.isEmpty(editUser.getPassword())) {
				String encPassword = CipherUtil.encrypt(editUser.getPassword());
				editUser.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.update(connection, editUser);

			commit(connection);

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}
}
