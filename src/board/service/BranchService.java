package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtils.*;

import java.sql.Connection;
import java.util.List;

import board.beans.Branch;
import board.dao.BranchDao;

public class BranchService {

	public List<Branch> getBranch(){

		Connection connection = null;

		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getBranch(connection);

			commit(connection);
			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}
}
