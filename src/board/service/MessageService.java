package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtils.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import board.beans.Message;
import board.beans.UserMessage;
import board.dao.MessageDao;
import board.dao.UserMessageDao;

public class MessageService {

	// SQL文に登録するメソッド
	public void register(Message message) {

		Connection connection = null;

		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();

			// SQL文に挿入処理
			messageDao.insert(connection, message);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	public void deleteMessage(int messageId) {

		Connection connection = null;

		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, messageId);

			commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage(String startDate, String endDate, String category) {

		Connection connection = null;

		try {
			connection = getConnection();

			String defaultStartDate = "2019-01-01 00:00:00";
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String defaultEndDate = sdf.format(timestamp);

            if(!StringUtils.isEmpty(startDate)) {
            	startDate += " 00:00:00";
            }else {
            	startDate = defaultStartDate;
            }

            if(!StringUtils.isEmpty(endDate)) {
            	endDate += " 23-59-59";
            }else {
            	endDate = defaultEndDate;
            }

			UserMessageDao MessageDao = new UserMessageDao();
			List<UserMessage> ret = MessageDao.getUserMessages(connection, LIMIT_NUM, startDate, endDate, category);


			commit(connection);

			return ret;

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;

		}catch(Error e) {
			rollback(connection);
			throw e;

		}finally {
			close(connection);
		}
	}
}
