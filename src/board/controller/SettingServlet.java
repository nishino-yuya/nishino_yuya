package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Branch;
import board.beans.Department;
import board.beans.User;
import board.exception.NoRowsUpdatedRuntimeException;
import board.service.BranchService;
import board.service.DepartmentService;
import board.service.UserService;

@WebServlet(urlPatterns= {"/setting"})
public class SettingServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {

		HttpSession session = request.getSession();
		String errorMessage = "不正なパラメータが入力されました";
		String strEditUserId = request.getParameter("userId");

		if(StringUtils.isEmpty(strEditUserId)) {
			session.setAttribute("errorMessages", errorMessage);

			response.sendRedirect("manage");
			return;
		}

		if(!strEditUserId.matches("^[0-9]+$")) {
			session.setAttribute("errorMessages", errorMessage);

			response.sendRedirect("manage");
			return;
		}

		int editUserId = Integer.parseInt(strEditUserId);
		User editUser = new UserService().getEditUser(editUserId);

		if(editUser == null) {
			request.setAttribute("errorMessages", errorMessage);

			response.sendRedirect("manage");
			return;
		}

		List<Branch> branches = new BranchService().getBranch();
		List<Department> departments = new DepartmentService().getDepartment();

		request.setAttribute("editUser", editUser);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if(isValid(messages, editUser)){
			try {
				new UserService().update(editUser);

			}catch(NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。" +
						"最新のデータが表示されています。データを確認してください。");

				request.setAttribute("eerorMessage", messages);
				request.setAttribute("editUser", editUser);

				request.getRequestDispatcher("setting.jsp").forward(request, response);
				return;
			}

			request.setAttribute("editUser", editUser);

			response.sendRedirect("manage");
		}else {
			List<Branch> branches = new BranchService().getBranch();
			List<Department> departments = new DepartmentService().getDepartment();

			request.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);

			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
		throws IOException, ServletException{

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setPasswordCheck(request.getParameter("passwordCheck"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return editUser;
	}

	protected boolean isValid(List<String> messages, User editUser) {

		isCheck("ログインID", 20, 6, editUser.getAccount(), messages);

		if(!editUser.getAccount().matches("^[0-9a-zA-Z]*$")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}

		UserService userSettingService = new UserService();
		User user = userSettingService.accountCheck(editUser.getAccount());

		if((user != null) && (user.getId() != editUser.getId())) {
			messages.add("ログインIDが重複しています");
		}

		isCheck("ユーザー名", 10, 0, editUser.getName(), messages);

		if((!StringUtils.isEmpty(editUser.getPassword())) || (!StringUtils.isEmpty(editUser.getPasswordCheck()))) {
			isCheck("パスワード", 20, 6, editUser.getPassword(), messages);
			if(!editUser.getPassword().matches("^[0-9a-zA-Z -~]*$")) {
				messages.add("パスワードは記号を含む半角英数字で入力してください");
			}

			isCheck("パスワード(確認用)", 20, 6, editUser.getPasswordCheck(), messages);

			if(!editUser.getPasswordCheck().matches("^[0-9a-zA-Z -~]*$")) {
				messages.add("パスワード(確認用)は記号を含む半角英数字で入力してください");
			}
		}

		if(messages.size() == 0 && !editUser.getPassword().equals(editUser.getPasswordCheck())) {
				messages.add("パスワードとパスワード(確認用)が一致していません");
		}

		if((editUser.getBranchId() == 1) && (editUser.getDepartmentId() >= 3)){
			messages.add("支店と部署・役職名の組み合わせが不適切です");
		}else if((editUser.getBranchId() != 1) && (editUser.getDepartmentId() <= 2)){
			messages.add("支店と部署・役職名の組み合わせが不適切です");
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

	protected void isCheck(String str, int upCount, int lowCount, String get, List<String> messages) {

		if(get.length() > upCount || get.length() < lowCount || StringUtils.isEmpty(get)) {
			if(lowCount == 0) {
				messages.add(str + "は" + upCount + "文字以下で入力してください");
			}else {
				messages.add(str + "は" + lowCount + "文字以上" + upCount + "文字以下で入力してください");
			}
		}
	}
}
