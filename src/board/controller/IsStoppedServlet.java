package board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.beans.User;
import board.service.UserService;


@WebServlet(urlPatterns= {"/isStopped" })
public class IsStoppedServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			 throws ServletException, IOException {

		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("userId")));
		user.setIsStopped(Integer.parseInt(request.getParameter("isStopped")));

		new UserService().changeStatus(user);

		response.sendRedirect("manage");
	}
}
