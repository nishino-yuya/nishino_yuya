package board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.service.MessageService;

@WebServlet(urlPatterns= {"/deleteMessage"})
public class DeleteMessageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {

		String strMessageId = request.getParameter("deleteMessage");
		int messageId = Integer.parseInt(strMessageId);
		new MessageService().deleteMessage(messageId);

		response.sendRedirect("./");
	}
}
