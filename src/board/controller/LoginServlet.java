package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.service.UserService;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		// 入力情報を取得して、データベースの中身と照合する
		UserService userloginService = new UserService();
		User user = userloginService.login(account, password);
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(isValid(request, user, messages)) {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		}
		else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("account", account);

			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}

	protected boolean isValid(HttpServletRequest request, User user, List<String> messages) {

		if(StringUtils.isEmpty(request.getParameter("account"))
					|| StringUtils.isEmpty(request.getParameter("password"))){
			messages.add("ログインIDまたはパスワードを入力してください");
		}else if(user == null) {
			messages.add("ログインIDまたはパスワードが誤っています");
		}else if(user.getIsStopped() == 1) {
			messages.add("停止されたアカウントです");
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}
}
