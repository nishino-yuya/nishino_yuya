package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Branch;
import board.beans.Department;
import board.beans.User;
import board.service.BranchService;
import board.service.DepartmentService;
import board.service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<User> users = new UserService().getUsers();
		List<Branch> branches = new BranchService().getBranch();
		List<Department> departments = new DepartmentService().getDepartment();
		HttpSession session = request.getSession();

		session.setAttribute("users", users);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String branchId = request.getParameter("branchId");
		String departmentId = request.getParameter("departmentId");
		int intBranchId  = Integer.parseInt(branchId);;
		int intDepartmentId  = Integer.parseInt(departmentId);
		User user = new User();

		user.setAccount(request.getParameter("account"));
		user.setName(request.getParameter("name"));
		user.setPassword(request.getParameter("password"));
		user.setPasswordCheck(request.getParameter("passwordCheck"));
		user.setBranchId(intBranchId);
		user.setDepartmentId(intDepartmentId);

		List<String> messages = new ArrayList<String>();
		// 入力が正常であれば、入力をインスタンスに格納する
		// validationが完了次第、setter をif文の外に出す
		if(isValid(user, messages)) {

			user.setIsStopped(0);

			new UserService().register(user);

			response.sendRedirect("manage");
		}else {
			List<Branch> branches = new BranchService().getBranch();
			List<Department> departments = new DepartmentService().getDepartment();

			request.setAttribute("errorMessages", messages);
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);

			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	// 必須項目の入力があるか確認処理
	private boolean isValid(User user, List<String> messages) {

		isCheck("ログインID", 20, 6, user.getAccount(), messages);

		if(!user.getAccount().matches("^[0-9a-zA-Z]*$")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}

		UserService userSignUpService = new UserService();
		User existingUser = userSignUpService.accountCheck(user.getAccount());

		if(existingUser != null) {
			messages.add("ログインIDが重複しています");
		}

		isCheck("名前", 10, 0, user.getName(), messages);

		isCheck("パスワード", 20, 6, user.getPassword(), messages);

		if(!user.getPassword().matches("^[0-9a-zA-Z-~]*$")) {
			messages.add("パスワードは記号を含む半角英数字で入力してください");
		}

		isCheck("パスワード(確認用)", 20, 6, user.getPasswordCheck(), messages);

		if(!user.getPasswordCheck().matches("^[0-9a-zA-Z-~]*$")) {
			messages.add("パスワード(確認用)は記号を含む半角英数字で入力してください");
		}

		if((!user.getPassword().equals(user.getPasswordCheck()))
				&& (!StringUtils.isEmpty(user.getPassword()))
					&& (!StringUtils.isEmpty(user.getPasswordCheck()))) {

			messages.add("パスワードとパスワード(確認用)が一致していません");
		}

		if(user.getBranchId() == 100) {
			messages.add("支店を選択してください");
		}

		if(user.getDepartmentId() == 100) {
			messages.add("部署・役職を選択してください");
		}

		if((user.getBranchId() == 1) && (user.getDepartmentId() >= 3)){
			messages.add("支店と部署・役職名の組み合わせが不適切です");
		}else if((user.getBranchId() != 1) && (user.getDepartmentId() <= 2)){
			messages.add("支店と部署・役職名の組み合わせが不適切です");
		}

		if(messages.size() == 0) {
			return true;

		}else {
			return false;
		}
	}

	private void isCheck(String str, int upCount, int lowCount, String get, List<String> messages) {

		if(StringUtils.isEmpty(get)) {
			messages.add(str + "を入力してください");
		}else if(get.length() > upCount || get.length() < lowCount) {
			if(lowCount == 0) {
				messages.add(str + "は" + upCount + "文字以下で入力してください");
			}else {
				messages.add(str + "は" + lowCount + "文字以上" + upCount + "文字以下で入力してください");
			}
		}
	}
}
