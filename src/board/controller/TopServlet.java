package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.UserComment;
import board.beans.UserMessage;
import board.service.MessageService;
import board.service.UserService;

@WebServlet(urlPatterns= {"/index.jsp"})
public class TopServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String category = request.getParameter("category");
		List<UserMessage> messages = new MessageService().getMessage(startDate, endDate, category);
		List<UserComment> comments = new UserService().getComment();
		String[] search = {startDate, endDate, category};
		HttpSession session = request.getSession();

		session.setAttribute("messages", messages);
		session.setAttribute("comments", comments);
		session.setAttribute("search", search);

		request.getRequestDispatcher("top.jsp").forward(request, response);
	}
}
