package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Message;
import board.beans.User;
import board.service.MessageService;

@WebServlet(urlPatterns = {"/message"})
public class MessageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Message message = new Message();
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		message.setTitle(title);
		message.setText(text);
		message.setCategory(category);

		// sessionの取得
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if(isValid(messages, message)) {
			// messageを格納するリストの作成
			User user = (User) session.getAttribute("loginUser");

			message.setUserId(user.getId());

			new MessageService().register(message);

			response.sendRedirect("./");
		}else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("message", message);

			request.getRequestDispatcher("message.jsp").forward(request, response);
		}
	}

	private boolean isValid(List<String> messages, Message message){

		isCheck("件名", 30, messages, message.getTitle() );
		isCheck("本文", 1000, messages, message.getText());
		isCheck("カテゴリー", 10, messages, message.getCategory());

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

	private void isCheck(String str, int strCount, List<String> messages, String get) {

		if(StringUtils.isBlank(get)) {
			messages.add(str + "を入力してください");
		}

		if(get.length() > strCount) {
			messages.add(str + "は" + strCount + "文字以下で入力してください");
		}
	}
}
