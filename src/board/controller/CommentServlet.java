package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Comment;
import board.beans.User;
import board.service.CommentService;

@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {

		String text = request.getParameter("comment");
		String strMessageId = request.getParameter("messageId");
		int messageId = Integer.parseInt(strMessageId);
		Comment comment = new Comment();
		List<String> messages = new ArrayList<String>();

		comment.setText(text);

		if(isValid(messages, comment)) {
			// 現在しているログインユーザーをセッションから取得し、Userに格納
			User user = (User) request.getSession().getAttribute("loginUser");

			comment.setUserId(user.getId());
			comment.setMessageId(messageId);

			new CommentService().register(comment);

			response.sendRedirect("./");
		}else {
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("./");
		}
	}

	private boolean isValid(List<String> messages, Comment comment){

		String text = comment.getText();

		if(StringUtils.isBlank(text)) {
			messages.add("コメントを入力してください");
		}

		if(text.length() > 500) {
			messages.add("コメントは500文字以内で入力してください");
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}
}
