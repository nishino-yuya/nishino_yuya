package board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.service.CommentService;

@WebServlet(urlPatterns={"/deleteComment"})
public class DeleteCommentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {

		String strCommentId = request.getParameter("deleteComment");
		int commentId = Integer.parseInt(strCommentId);
		new CommentService().deleteComment(commentId);

		response.sendRedirect("./");
	}
}
