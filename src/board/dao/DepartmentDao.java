package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.Department;
import board.exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> getDepartment(Connection connection){

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM departments";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Department> ret = toDepartmentList(rs);

			return ret;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}
	}

	public List<Department> toDepartmentList(ResultSet rs) throws SQLException{

		List<Department> ret = new ArrayList<Department>();

		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Department department = new Department();

				department.setId(id);
				department.setName(name);
				department.setCreatedDate(createdDate);
	            ret.add(department);
			}
			return ret;

		}finally {
			close(rs);
		}
	}
}
