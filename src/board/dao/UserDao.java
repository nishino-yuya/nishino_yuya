package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.exception.NoRowsUpdatedRuntimeException;
import board.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_stopped");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // pass
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); //department_id
            sql.append(", ?"); //is_stopped
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());
            ps.setInt(6, user.getIsStopped());
            ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}
	}

	public void update(Connection connection, User editUser) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" account = ?");

			if(!StringUtils.isEmpty(editUser.getPassword())) {
				sql.append(", password = ?");
			}

			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editUser.getAccount());

			// パスワードの入力がなければ
			if(StringUtils.isEmpty(editUser.getPassword())) {
				ps.setString(2, editUser.getName());
				ps.setInt(3, editUser.getBranchId());
				ps.setInt(4, editUser.getDepartmentId());
				ps.setInt(5, editUser.getId());
			}else {
				ps.setString(2, editUser.getPassword());
				ps.setString(3, editUser.getName());
				ps.setInt(4, editUser.getBranchId());
				ps.setInt(5, editUser.getDepartmentId());
				ps.setInt(6, editUser.getId());
			}

			int count = ps.executeUpdate();

			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);

	    } finally {
	    	close(ps);
		}
	}

	public void changeStatus(Connection connection, User user) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" is_stopped = ?");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsStopped());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();

			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);

	    } finally {
	    	close(ps);
		}
	}

	public User getUser(Connection connection, String account, String password) {

		PreparedStatement ps = null;

		try {
			String sql = "select * from users where account=? and password=?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if(userList.isEmpty()) {
				return null;
			}else if(userList.size() >= 2) {
				throw new IllegalStateException("userList.size() >= 2");
			}else {
				return userList.get(0);
			}

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}
	}

	public User accountCheck(Connection connection, String account) {

		PreparedStatement ps = null;

		try {
			String sql = "select * from users where account=?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if(userList.isEmpty()) {
				return null;
			}else if(userList.size() >= 2) {
				throw new IllegalStateException("userList.size() >= 2");
			}else {
				return userList.get(0);
			}

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();

		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				int isStopped = rs.getInt("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");
	            Timestamp updatedDate = rs.getTimestamp("updated_date");
	            User user = new User();

	            user.setId(id);
	            user.setAccount(account);
	            user.setPassword(password);
	            user.setName(name);
	            user.setBranchId(branchId);
	            user.setDepartmentId(departmentId);
	            user.setIsStopped(isStopped);
	            user.setCreatedDate(createdDate);
	            user.setUpdatedDate(updatedDate);

	            ret.add(user);
			}
			return ret;

		}finally {
			close(rs);
		}
	}

	public List<User> getUsers(Connection connection, int num){

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);

			return ret;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}
	}

	public List<String> getUsersAccounts(Connection connection, int num){

		PreparedStatement ps = null;

		try {
			String sql = "SELECT account FROM users";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<String> ret = toUserAccountList(rs);

			return ret;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}
	}

	private List<String> toUserAccountList(ResultSet rs) throws SQLException {

		List<String> ret = new ArrayList<String>();

		try {
			while(rs.next()) {
				String account = rs.getString("account");
	            User user = new User();

	            user.setAccount(account);
	            ret.add(user.getAccount());
			}
			return ret;

		}finally {
			close(rs);
		}
	}

	public User getEditUser(Connection connection, int editUserId) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, editUserId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if(userList.isEmpty()) {
				return null;
			}else if(userList.size() >= 2) {
				throw new IllegalStateException("userList.size() >= 2");
			}else {
				return userList.get(0);
			}

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}
	}
}
