package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import board.beans.UserMessage;
import board.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages
					(Connection connection, int num, String startDate, String endDate, String category){

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date BETWEEN ? AND ? ");// 1が開始時刻、2が現在時刻

            if(!StringUtils.isEmpty(category)) {
            	sql.append("AND messages.category LIKE ? ");
            }

            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startDate);
            ps.setString(2, endDate);

            if(!StringUtils.isEmpty(category)) {
            	category = "%" + category + "%";
            	ps.setString(3, category);
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);

            return ret;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}
	}

	public List<UserMessage> toUserMessageList(ResultSet rs)throws SQLException{

		List<UserMessage> ret = new ArrayList<UserMessage>();

		try {
			while(rs.next()) {
				String account = rs.getString("account");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				Timestamp createdDate = rs.getTimestamp("created_date");
				UserMessage message = new UserMessage();

				message.setId(id);
				message.setAccount(account);
				message.setName(name);
				message.setTitle(title);
				message.setText(text);
				message.setCategory(category);
				message.setUserId(userId);
				message.setCreatedDate(createdDate);

				ret.add(message);
			}
			return ret;

		}finally {
			close(rs);
		}
	}
}
